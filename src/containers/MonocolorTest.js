/**
 * Monocolor display
 * 
 * The display renders a full screen of 
 */

import React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Surface } from 'gl-react-dom';
import AdMonoTestShader from '../components/AdMonoTestShader';

const styles = theme => ({
    fullScreen: {
        position: 'fixed',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    }
});

class MonocolorTest extends React.Component {
    render() {
        const { classes, displayConfiguration } = this.props;
        const testConfig = displayConfiguration.monocolorTest;
        return (
            <div className={classes.fullScreen}
                style={{background: testConfig.background.hex}}>
                <Surface className={classes.fullScreen}
                    width={displayConfiguration.width}
                    height={displayConfiguration.height}>
                    <AdMonoTestShader
                        r={testConfig.background.rgb.r/255}
                        g={testConfig.background.rgb.g/255}
                        b={testConfig.background.rgb.b/255}
                        contrast={testConfig.contrast}
                        brightness={testConfig.brightness}
                        >
                    </AdMonoTestShader>
                </Surface>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        displayConfiguration: state.displayConfiguration
    };
}

export default connect(mapStateToProps)(
    withStyles(styles)(MonocolorTest)
);
