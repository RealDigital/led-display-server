import React from 'react';
import { connect } from 'react-redux';
import AdContents from './AdContents';
import MonocolorTest from './MonocolorTest';

class Display extends React.Component {
    componentDidMount() {
        // Disable scrolling
        document.body.style.overflow = "hidden";
    }

    render() {
        const { displayConfiguration } = this.props;

        return (
            displayConfiguration.testMode === 'off' ?
                    <AdContents></AdContents> : <MonocolorTest></MonocolorTest>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        displayConfiguration: state.displayConfiguration,
        content: state.content
    };
}

export default connect(mapStateToProps)(Display)
