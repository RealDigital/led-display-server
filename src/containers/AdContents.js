'use strict';

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import moment from 'moment-timezone';
import AdContentDisplay from '../components/AdContentDisplay';
import ClockDisplay from './ClockDisplay';
import AdDownload from './AdDownload';

const styles = theme => ({
    fullScreen: {
        position: 'fixed',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    }
});

class AdDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.displayContents = [];
        this.timeElapsed = 0;
        this.contentIndex = 0;
    }

    componentDidMount() {
        this.updateDisplayContents();
        // Invoke content updater every second.
        this.interval = setInterval(
            this.handleIntervalUpdate,
            1000
        )
        this.updateLedControl();
    }

    componentWillUnmount() {
        if(this.interval) {
            clearInterval(this.interval);
        }

        if(this.timeBasedInterval) {
            clearInterval(this.timeBasedInterval)
        }
    }

    displayUpdateListener = (event, arg) => {
        console.log(arg);

        if (arg.adImages.length > 0) {
            this.setState(Object.assign({}, arg, {
                displayItem: arg.adImages[0],
                imgLabel: arg.adImages[0].label,
                curIndex: 0,
                curSec: 0
            }))
        }
    }

    // The following handler updates the brightness and contrast
    // of the screen according to settings.
    updateLedControl = () => {
        const { ledControl } = this.props;
        if(this.timeBasedInterval) {
            clearInterval(this.timeBasedInterval);
            this.timeBasedInterval = null;
        }

        switch(ledControl.mode) {
            case 'constant':
            this.setContentsContrastBrightness(
                ledControl.constant.contrast,
                ledControl.content.brightness
            );
            break;
            case 'timeBased':
                this.timeBasedInterval = setInterval(
                    this.timeBasedLedControlHandler, 
                    ledControl.updateInterval * 1000
                    );
                this.timeBasedLedControlHandler();
                return;
            default:
                this.setContentsContrastBrightness(1., 1.);
                return;
        }
    }

    setContentsContrastBrightness(contrast, brightness) {
        for(var i = 0; i < this.displayContents.length; i++) {
            if(this.displayContents[i]) {
                this.displayContents[i].setShaderParams(
                    contrast, brightness
                );    
            }
        }
    }

    timeBasedConfigMapFunction = ({ time, brightness, contrast }) => ({
        time: moment(time, "HH:mm"),
        brightness,
        contrast
    });

    timeBasedLedControlHandler = () => {
        const { ledControl } = this.props;
        let config = ledControl.timeBased.map(this.timeBasedConfigMapFunction);

        // Create time series and interpolate
        config = [
            Object.assign({}, config[config.length - 1], {
                time: moment(
                    config[config.length - 1].time, "HH:mm"
                ).subtract(1, 'days')
            }),
            ...config,
            Object.assign({}, config[0], {
                time: moment(config[0].time, "HH:mm").add(1, 'days')
            })
        ];

        const current = moment(moment().format("HH:mm"), "HH:mm");
        let before = 0;
        let after = 0;
        for(var i = 0; i < config.length; i++) {
            if (config[i].time.isSame(current) || 
                    config[i].time.isBefore(current)) {
                before = i;
            }
            if(config[i].time.isAfter(current)) {
                after = i;
                break;
            }
        }

        const timeToBefore = config[before].time.diff(current, 'minutes');
        const timeToAfter = current.diff(config[after].time, 'minutes');
        const percentOfBefore = timeToAfter / (timeToBefore + timeToAfter);
        const brightness = percentOfBefore * config[before].brightness + (1 - percentOfBefore) * config[after].brightness;
        const contrast = percentOfBefore * config[before].contrast + (1 - percentOfBefore) * config[after].contrast;
        
        if(brightness !== this.brightness || contrast !== this.contrast) {
            this.setContentsContrastBrightness(contrast, brightness);
            this.brightness = brightness;
            this.contrast = contrast;
        }
    }

    updateContentState = (prevState) => {
        const { content } = this.props;
        const adContents = content.adContents;

        let contentIndex = prevState.contentIndex;
        let timeElapsed = prevState.timeElapsed + 1;
        let duration = prevState.contentDuration;
        if (timeElapsed > duration) {
            timeElapsed = 0;
            contentIndex++;
            if (contentIndex >= adContents.length) {
                contentIndex = 0;
            }

            const adItem = adContents[contentIndex];
            URL.revokeObjectURL(prevState.content);
            return {
                content: URL.createObjectURL(adItem.blob),
                contentType: adItem.type,
                contentIndex: contentIndex,
                contentDuration: adItem.duration,
                timeElapsed: timeElapsed
            };
        } else {
            return {
                timeElapsed: timeElapsed
            };
        }
    };

    // The following handler updates the link for the current on-display 
    // ad content. The handler is invoked every second.
    handleIntervalUpdate = () => {
        const { content } = this.props;

        const adContents = content.adContents;
        if(adContents.length === 0) {
            return;
        }

        //this.setState(this.updateContentState);
        this.timeElapsed += 1;
        let duration = adContents[this.contentIndex].duration;
        if (this.timeElapsed > duration) {
            this.timeElapsed = 0;
            this.contentIndex++;
            if (this.contentIndex >= adContents.length) {
                this.contentIndex = 0;
            }

            this.updateDisplayContents();
        }
    }

    updateDisplayContents = () => {
        if(this.contentIndex < this.displayContents.length) {
            for(let i = 0; i < this.displayContents.length; i++) {
                if(i === this.contentIndex) {
                    this.displayContents[i].show();
                } else {
                    this.displayContents[i].hide();
                }
            }
        }
    }

    displayContentRefCallback = (item) => {
        this.displayContents.push(item);
    }

    render() {
        const { classes, displayConfiguration, content } = this.props;

        return (
            <div className={classes.fullScreen}>
                {
                    content.adContents.map((item, index) => {
                        return (
                            <AdContentDisplay 
                                innerRef={this.displayContentRefCallback}
                                key={"Display_" + index.toString()}
                                contentType={item.type}
                                contentUrl={URL.createObjectURL(item.blob)}
                                width={displayConfiguration.width}
                                height={displayConfiguration.height}
                            ></AdContentDisplay>
                        )
                    })
                }
                {/* <ClockDisplay 
                    width={displayConfiguration.width}
                    height={displayConfiguration.height}
                    format={content.timeFormat}
                    ticking={true}
                    timezone={content.timezone}
                    visible={content.showTime}
                    font={[
                        content.timeTextFontWeight,
                        content.timeTextFontSize + 'px',
                        content.timeTextFontFamily
                    ].join(' ')}
                    strokeWidth={parseInt(content.timeTextStrokeWidth)}
                    strokeStyle={content.timeTextStrokeColor}
                    fillStyle={content.timeTextFillColor}
                    textBaseline={content.timeTextBaseline}
                    textAlign={content.timeTextAlign}
                    textMarginX={parseInt(content.timeTextMarginX)}
                    textMarginY={parseInt(content.timeTextMarginY)}>
                </ClockDisplay> */}
                <AdDownload visible={false} updateInterval={1000}></AdDownload>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        displayConfiguration: state.displayConfiguration,
        content: state.content,
        ledControl: state.ledControl
    };
}

export default connect(mapStateToProps)(
    withStyles(styles)(AdDisplay)
);
