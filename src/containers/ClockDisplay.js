import React from 'react';
import PropTypes from 'prop-types';
import JSON2D from 'react-json2d';
import { LinearCopy } from 'gl-react';
import moment from 'moment-timezone';

const BASE_UNIT = 'milliseconds';

class ClockDisplay extends React.Component {
    constructor(props) {
        super(props);

        const date = props.date || props.children || null;
        const timesatmp = moment();
        const baseTime = date ? moment(new Date(date).getTime()) : timesatmp;

        this.state = {
            realTime: !date,
            now: baseTime,
            baseTime,
            startTime: timesatmp
        };
    }

    componentDidMount() {
        const { ticking, interval } = this.props;

        if(ticking && interval) {
            this.tickTimer = setInterval(() => {
                this.updateClock();
            }, interval);
        }
    }

    componentWillUnmount() {
        if (this.tickTimer) {
            clearInterval(this.tickTimer);
        }
    }

    updateClock() {
        const { realTime } = this.state;

        if(realTime) {
            this.setState({
                now: moment()
            });
        } else {
            const { baseTime, startTime } = this.state;
            const newTime = moment();
            const diff = newTime.diff(startTime, BASE_UNIT);

            this.setState({
                now: baseTime.clone().add(diff, BASE_UNIT)
            });
        }
    }

    render() {
        const { format, timezone, width, height,
            font, fillStyle, strokeStyle, strokeWidth,
            textBaseline, textAlign, 
            textMarginX, textMarginY, visible} = this.props;
        const { now } = this.state;
        const localizedTime = now;

        if (timezone) {
            localizedTime.tz(timezone);
        }

        const formattedTime = (visible) ? localizedTime.format(format) : "";

        const y = (textBaseline === "bottom") ? height - textMarginY:
            (textBaseline === "middle") ? height / 2 : textMarginY;
        const x = (textAlign === "right") ? width - textMarginX:
            (textAlign === "center") ? width / 2 : textMarginX;

        return (
            <LinearCopy>
                <JSON2D width={width} height={height}>
                {{
                    size: [width, height],
                    background: "transparent",
                    draws: [{}, [
                        'clearRect', 0, 0, width, height
                    ], {
                        font: font,
                        fillStyle: fillStyle,
                        textBaseline: textBaseline,
                        textAlign: textAlign
                    },[
                        'fillText',
                        formattedTime,
                        x,
                        y
                    ], {
                        font: font,
                        lineWidth: strokeWidth,
                        strokeStyle: strokeStyle,
                        textBaseline: textBaseline,
                        textAlign: textAlign
                    }, [
                        'strokeText',
                        formattedTime,
                        x,
                        y
                    ],]
                }}
                </JSON2D>
            </LinearCopy>
        )
    }
}

ClockDisplay.propTypes = {
    font: PropTypes.string,
    fillStyle: PropTypes.string,
    strokeStyle: PropTypes.string,
    strokeWidth: PropTypes.number,
    textBaseline: PropTypes.string,
    textAlign: PropTypes.string,
    textMarginX: PropTypes.number,
    textMarginY: PropTypes.number,
    visible: PropTypes.bool
}

ClockDisplay.defaultProps = {
    font: "bold 78px Didot,Georgia,serif",
    interval: 1000,
    fillStyle: "#fff",
    strokeStyle: "#000",
    strokeWidth: 3,
    textBaseline: "top",
    textAlign: "left",
    textMarginX: 10,
    textMarginY: 10,
    visible: true
}

export default ClockDisplay;
