import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import LinearProgress from '@material-ui/core/LinearProgress';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import red from '@material-ui/core/colors/red';
import amber from '@material-ui/core/colors/amber';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Logging from '../api/logging';
import { loadContent } from '../actions/content';

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1
    }
});

class AdDownloadDialog extends React.Component {
    state = {
        status: 'idle',
        activeStep: 0,
        err: undefined,
        steps: [
            "Parsing display.json",
            "Downloading display configuration and images"
        ],
        folderKey: undefined,
        filesToDownload: {},
        filesDownloaded: 0,
        stepMessages: [
            [], []
        ],
        contents: {}
    }

    // When the compoment is mounted, if the `updateInterval` property is not 
    // zero, it starts periodically checking for content changes from AWS S3 
    // storage.
    componentDidMount() {
        const { updateInterval, awsConfig } = this.props;

        this.bucketLink = "https://" + awsConfig.bucket + ".s3.amazonaws.com";

        if(updateInterval !== 0) {
            this.updateInterval = setInterval(
                this.updateDisplay,
                Math.max(1000, updateInterval)
            )
        }
    }

    componentWillUnmount() {
        if(this.updateInterval) {
            clearInterval(this.updateInterval)
        }
    }

    checkForDownload = (data) => {
        if(data.current !== this.state.folderKey) {
            this.startDownload();
        }
    }

    parseJsonResponse(response) {
        if(!response.ok) { throw response; }
        return response.json();
    }

    errorCallback(err) {
        Logging.error(err.name + ': ' + err.message);
    }

    updateDisplay = () => {
        // Check if it is online
        fetch(this.bucketLink + '/display.json',
            { cache: "no-cache", redirect: 'follow' })
            .then(this.parseJsonResponse)
            .then(this.checkForDownload)
            .catch(this.errorCallback)
    }

    startDownload() {
        // Set state tu start.
        this.setState({
            status: 'start',
            filesDownloaded: 0
        })

        // Fetch display.json.
        // The file contains the folder name of the ad contents.
        fetch(
            this.bucketLink + '/display.json',
            { cache: "no-cache", redirect: 'follow' }
            ).then((response) => {
                return response.json();
            })
            .then(this.queryConfiguration)
            .catch((err) => {
                this.abortWithError(
                    err, 'Failed to get display.json from S3 server\n'
                    );
                Logging.error(
                    "Failed to get display.json from S3 server with error " + 
                    err.name + ": " + err.message
                    );
            })
    }

    // After the display.json is successfully loaded and parsed, try to get the 
    // `config.json` file for the current display configuration.
    queryConfiguration = (data) => {
        let folderKey = data.current
        this.setState({
            folderKey: folderKey
        })
        Logging.info('Loading config.json from folder ' + folderKey);
        fetch(
            this.bucketLink + '/' + folderKey + 'config.json', 
            { cache: "no-cache", redirect: 'follow' }
        ).then((response) => {
            return response.json();
        }).then(
            this.parseConfiguration
        ).catch((err) => {
            this.abortWithError(err, 'Failed to get config.json from S3 server\n');
            Logging.error(
                "Failed to get config.json from S3 server with error " + 
                err.name + ": " + err.message
                );
        })
    }

    // Parse `config.json` and prepare to download the contents (including 
    // images and videos).
    parseConfiguration = (data) => {
        let filesToDownload = {}
        data.adContents.forEach((adItem, index) => {
            const filename = adItem.filename
            const ext = /(?:\.([^.]+))?$/.exec(filename)[1]
            if (ext) {
                filesToDownload[filename] = {
                    fileURL: this.bucketLink + '/' + this.state.folderKey + filename,
                    isDownloaded: false,
                    ext: ext,
                    blob: undefined
                }
            }
        })

        this.setState((prevState) => ({
            status: 'downloading',
            activeStep: prevState.activeStep + 1,
            contents: Object.assign({}, prevState.contents, data),
            filesToDownload: filesToDownload
        }))

        for(var filename in this.state.filesToDownload) {
            const item = this.state.filesToDownload[filename];
            fetch(item.fileURL, { cache: "no-cache", redirect: 'follow' })
                .then((response) => {
                    return response.blob()
                })
                .then(this.downloadEachFile(filename))
                .catch(this.reportFileDownloadError(filename))
        }
    }

    // Returns a function that will be invoked when the corresponding file is // downloaded successfully.
    downloadEachFile = filename => data => {
        this.setState((prevState) => {
            const item = prevState.filesToDownload[filename];
            item.isDownloaded = true;
            item.blob = data;
            return {
                filesDownloaded: prevState.filesDownloaded + 1,
                filesToDownload: Object.assign({}, prevState.filesToDownload)
            }
        })

        this.logMessage(1, 'success', 
            'Successfully downloaded file ' + filename);
        Logging.info('File ' + filename + ' is fetched successfully.');
    }

    // Handles the error when the corresponding file cannot be fetched.
    reportFileDownloadError = filename => err => {
        this.abortWithError(err, 'Failed to get file ' + filename + ' from S3 server\n');
        Logging.error(
            'Failed to fetch file ' + filename + 
            ' with error ' + err.name + ': ' + err.message
            );
    }

    // When the component state is changed, the `componentDidUpdate` function 
    // is invoked. Here, the callback is used to determining all images/videos 
    // are downloaded from the AWS S3 storage.
    componentDidUpdate() {
        if (this.state.status === 'downloading') {
            if(this.state.filesDownloaded === 
                Object.keys(this.state.filesToDownload).length) {
                this.setState({
                    status: 'compiling'
                })
                this.updateDisplayConfiguration();
            }
        }
    }

    // Update content of the redux store.
    updateDisplayConfiguration = () => {
        console.log('Update Display Configuration')
        console.log(this.state)
        let adContents = []
        this.state.contents.adContents.forEach((item, index) => {
            console.log(item);
            console.log(this.state);
            adContents.push(Object.assign({}, item, {
                index: index,
                blob: this.state.filesToDownload[item.filename].blob,
                ext: this.state.filesToDownload[item.filename].ext,
                filename: undefined,
                type: this.state.filesToDownload[item.filename].blob.type
            }))
        })
        let contents = Object.assign({}, this.state.contents, {
            adContents: adContents
        })

        this.setState((prevState) => ({
            contents: contents,
            status: 'done',
            activeStep: prevState.activeStep + 1
        }))

        if(this.updateInterval) {
            this.dispatchLoadContent(contents)
            this.setState({
                status: 'idle'
            })
        }
    }

    dispatchLoadContent(display) {
        const { dispatch } = this.props;
        dispatch(loadContent(display));
    }

    logMessage(step, msgType, msgText) {
        this.setState((prevState) => {
            prevState.stepMessages[step].push({
                type: msgType,
                text: msgText
            })
            return {
                stepMessages: [
                    [...prevState.stepMessages[0]],
                    [...prevState.stepMessages[1]]
                ]
            }
        })
    }

    abortWithError = (err, message) => {
        this.setState({
            status: 'idle',
            err: err
        })
        this.logMessage(this.state.activeStep, 'error', message + err);
    }

    handleClose = (e) => {
        this.dispatchDisplayState(this.state.contents);
        this.setState({
            status: 'idle'
        })
    }

    getMessageColor = (messageType) => {
        switch (messageType) {
            case "error":
                return red[500];
            case "warning":
                return amber[500];
            default:
                return "inherit";
        }
    }

    getMessageIcon = (messageType) => {
        switch (messageType) {
            case "error":
                return "exclamation-circle";
            case "warning":
                return "exclamation-triangle";
            default:
                return "info-circle";
        }
    }

    render() {
        const { classes, visible } = this.props;

        return (
            <Dialog
                fullScreen
                open={visible && (this.state.status !== 'idle')}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton color="inherit"
                            disabled={this.state.status !== 'done'}
                            onClick={this.handleClose}>
                            <FontAwesomeIcon icon="times"></FontAwesomeIcon>
                        </IconButton>
                        <Typography variant="h6" color="inherit"
                            className={classes.flex}>
                            Download Ad Contents from Amazon S3 Storage
                            </Typography>
                        <Button color="inherit"
                            disabled={this.state.status !== 'done'}
                            onClick={this.handleClose}>
                            Close
                            </Button>
                    </Toolbar>
                </AppBar>
                <Stepper orientation="vertical" 
                    activeStep={this.state.activeStep}>
                    <Step>
                        <StepLabel>Parsing display.json</StepLabel>
                        <StepContent>
                            {
                                this.state.stepMessages[0].map(
                                    (message, index) => (
                                    <ListItem key={index} color={
                                        this.getMessageColor(message.type)
                                    }>
                                        <ListItemIcon>
                                            <FontAwesomeIcon icon={
                                                this.getMessageIcon(message.type)
                                            }></FontAwesomeIcon>
                                        </ListItemIcon>
                                        <ListItemText>
                                            {message.text}
                                        </ListItemText>
                                    </ListItem>
                                ))
                            }
                        </StepContent>
                    </Step>
                    <Step>
                        <StepLabel>
                            Fetching display configuration and images.
                        </StepLabel>
                        <StepContent>
                            <LinearProgress color="secondary"  
                                variant="determinate" 
                                value={
                                    this.state.filesDownloaded * 100 / Object.keys(this.state.filesToDownload).length
                                    } />
                            <List> {
                                this.state.stepMessages[1].map(
                                    (message, index) => (
                                    <ListItem key={index} color={
                                        this.getMessageColor(message.type)
                                    }>
                                        <ListItemIcon>
                                            <FontAwesomeIcon icon={
                                                this.getMessageIcon(message.type)
                                            }></FontAwesomeIcon>
                                        </ListItemIcon>
                                        <ListItemText>
                                            {message.text}
                                        </ListItemText>
                                    </ListItem>
                                ))
                            } </List>
                        </StepContent>
                    </Step>
                </Stepper>
            </Dialog>
        );
    }
}

AdDownloadDialog.propTypes = {
    updateInterval: PropTypes.number.isRequired,
    visible: PropTypes.bool
}

AdDownloadDialog.defaultProps = {
    visible: false
}

const mapStateToProps = (state) => {
    return {
        display: state.display,
        account: state.account,
        awsConfig: state.awsConfig
    };
}

export default connect(mapStateToProps)(
    withStyles(styles)(AdDownloadDialog)
);
