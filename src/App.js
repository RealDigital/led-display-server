import React, { Component } from 'react';
import { applyMiddleware, compose, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { throttle } from 'lodash';
import { library } from '@fortawesome/fontawesome-svg-core';
import { 
    faTimes, faExclamationCircle, faExclamationTriangle, faInfoCircle 
} from '@fortawesome/free-solid-svg-icons';
import { saveState, loadState } from './api/localStorage';
import rootReducer from './reducers';
import Display from './containers/Display';

library.add(faTimes, faExclamationCircle, faExclamationTriangle, faInfoCircle);

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadState();

const store = createStore(
    rootReducer(),
    (persistedState === undefined) ? undefined : {
        awsConfig: persistedState.awsConfig,
        display: persistedState.display,
        ledControl: persistedState.ledControl,
        configuration: persistedState.configuration
    },
    composeEnhancer(
        applyMiddleware(thunk)
    )
)

store.subscribe(throttle(() => {
    const storeState = store.getState();
    saveState(storeState);
}, 1000));

const theme = createMuiTheme({
    palette: {
        type: 'dark',
    },
    typography: {
        useNextVariants: true,
    },
});

class App extends Component {
    render() {
        // Use strict mode to highlighting potential problems in an application.
        return (
            <React.StrictMode>
                <MuiThemeProvider theme={theme}>
                    <CssBaseline />
                    <Provider store={store}>
                        <Display></Display>
                    </Provider>
                </MuiThemeProvider>
            </React.StrictMode>
        );
    }
}
  
export default App;
