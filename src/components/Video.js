import React, { Component } from "react";
import raf from "raf";

// We implement a component <Video> that is like <video>
// but provides a onFrame hook so we can efficiently only render
// if when it effectively changes.
export default class Video extends Component {
    constructor(props) {
        super(props);
        this.video = React.createRef();
    }

    loop = () => {
        this._raf = raf(this.loop);
        if (!this.video) return;
        const currentTime = this.video.current.currentTime;
        // Optimization that only call onFrame if time changes
        if (currentTime !== this.currentTime) {
            this.currentTime = currentTime;
            this.props.onFrame(currentTime);
        }
    };

    componentDidMount() {
        this.startPlay();
    }

    componentWillUnmount() {
        if(this._raf) {
            raf.cancel(this._raf);
            this._raf = null;
        }
    }

    startPlay() {
        if(this.video) {
            this.video.current.currentTime = 0;
            this.video.current.play();
            this._raf = raf(this.loop);
        }
    }

    stopPlay() {
        if(this.video) {
            this.video.current.pause();
        }
        if(this._raf) {
            raf.cancel(this._raf);
            this._raf = null;
        }
    }

    render() { 
        const { onFrame, ...rest } = this.props;
        return <video {...rest} ref={this.video} />;
    }
}
