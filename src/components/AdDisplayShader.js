import React from 'react';
import PropTypes from 'prop-types';
import { Node, Shaders, GLSL } from 'gl-react';

const shaders = Shaders.create({
    adDisplayShader: {
        frag: GLSL`precision highp float;

uniform sampler2D img;
varying vec2 uv;
uniform float contrast;
uniform float brightness;

void main() {
    vec4 imgVec = texture2D(img, uv);
    vec4 c = imgVec;
    vec3 c_contrasted = clamp((c.rgb - 0.5) * contrast + 0.5, 0.0, 1.0);
    vec3 c_moved = c_contrasted + clamp((1.0 - contrast) / 2.0, 0.0, 1.0);
    vec3 c_scaled = c_moved * brightness;
    gl_FragColor = vec4(c_scaled, c.a);
}`
    }
})

class AdDisplayShader extends React.Component {
    constructor(props) {
        super(props);
        this.node = React.createRef();
        this.contrast = 1.;
        this.brightness = 1.;
    }

    state = {
        contrast: 1.,
        brightness: 1.
    }

    componentDidMount() {
        const { contrast, brightness } = this.props;
        this.contrast = contrast;
        this.brightness = brightness;
    }

    redraw() {
        if(this.node.current) {
            this.node.redraw();
        }
    }

    setShaderParams(contrast, brightness) {
        // this.contrast = contrast;
        // this.brightness = brightness;
        // if(this.node.current) {
        //     this.node.current.redraw();
        // }
        this.setState({
            contrast: contrast,
            brightness: brightness
        })
    }

    render() {
        const { children } = this.props;

        return (
            <Node ref={this.node}
                shader={shaders.adDisplayShader}
                uniforms={{
                    img: children, 
                    contrast: this.state.contrast, 
                    brightness: this.state.brightness}}>
            </Node>
        );
    }
}

AdDisplayShader.propTypes = {
    contrast: PropTypes.number,
    brightness: PropTypes.number,
}

AdDisplayShader.defaultProps = {
    contrast: 1,
    brightness: 1
}

export default AdDisplayShader;
