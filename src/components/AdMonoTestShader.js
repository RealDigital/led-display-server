import React from 'react';
import PropTypes from 'prop-types';
import { Node, Shaders, GLSL } from 'gl-react';

const shaders = Shaders.create({
    adDisplayShader: {
        frag: GLSL`precision highp float;

uniform float r;
uniform float g;
uniform float b;
uniform float contrast;
uniform float brightness;

void main() {
    vec3 c_rgb = vec3(r, g, b);
    vec3 c_contrasted = clamp((c_rgb - 0.5) * contrast + 0.5, 0.0, 1.0);
    vec3 c_moved = c_contrasted + clamp((1.0 - contrast) / 2.0, 0.0, 1.0);
    vec3 c_scaled = c_moved * brightness;
    gl_FragColor = vec4(c_scaled, 1.0);
}`
    },
})

class AdMonoTestShader extends React.Component {
    render() {
        const { r, g, b, contrast, brightness } = this.props;
        return (
            <Node
                shader={shaders.adDisplayShader}
                uniforms={{ r, g, b, contrast, brightness }}>
            </Node>
        );
    }
}

AdMonoTestShader.propTypes = {
    contrast: PropTypes.number,
    brightness: PropTypes.number,
    r: PropTypes.number.isRequired,
    g: PropTypes.number.isRequired,
    b: PropTypes.number.isRequired
}

AdMonoTestShader.defaultProps = {
    contrast: 1,
    brightness: 1
}

export default AdMonoTestShader;
