import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Surface } from 'gl-react-dom';
import AdDisplayShader from '../components/AdDisplayShader';
import Video from '../components/Video';

const styles = theme => ({
    fullScreen: {
        position: 'fixed',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    }
});

class AdContentDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.wrapper = React.createRef();
        this.shader = React.createRef();
        this.video = React.createRef();
    }

    componentDidMount() {
        const { contentType, contentUrl } = this.props;
        console.log(contentType);
        console.log(contentUrl);
        console.log(this.video);
        console.log(this.wrapper);
    }

    hide() {
        if(this.wrapper) {
            this.wrapper.current.style.visibility = 'hidden';
        }
        if(this.video.current) {
            this.video.current.stopPlay();
        }
    }

    show() {
        if(this.wrapper) {
            this.wrapper.current.style.visibility = 'visible';
        }
        if(this.video.current) {
            this.video.current.startPlay();
        }
    }

    setShaderParams(contrast, brightness) {
        if(this.shader.current) {
            this.shader.current.setShaderParams(contrast, brightness);
        }
    }

    render() {
        const { classes, width, height, contentType, contentUrl } = this.props;

        return (
            <div ref={this.wrapper} className={classes.fullScreen}>
                <Surface
                    width={width} 
                    height={height}> 
                    <AdDisplayShader ref={this.shader}>
                        {
                            (/^(video)\//.test(contentType)) ? 
                            redraw => (
                                <Video ref={this.video}
                                    onFrame={redraw} autoPlay playsInline loop>
                                    <source type={contentType}
                                        src={contentUrl}>
                                    </source>
                                </Video>
                            ) : contentUrl
                        }
                    </AdDisplayShader>
                </Surface>
            </div>
        )
    }
}

export default withStyles(styles)(AdContentDisplay);
