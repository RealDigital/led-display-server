/**
 * APIs for sending messages to the main process.
 */
const { ipcRenderer } = window.require('electron');

class Logging {
    static warn(message) {
        ipcRenderer.send('logMessage', {
            type: 'warn',
            message: message
        });
    }

    static info(message) {
        ipcRenderer.send('logMessage', {
            type: 'info',
            message: message
        });
    }

    static debug(message) {
        ipcRenderer.send('logMessage', {
            type: 'debug',
            message: message
        });
    }

    static error(message) {
        ipcRenderer.send('logMessage', {
            type: 'error',
            message: message
        });
    }
}

export default Logging;
