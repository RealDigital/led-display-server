import { ipcMain } from 'electron';
import moment from 'moment-timezone';

/**
 * Server logging service
 */

const messages = [];

class Logging {
    init() {
        ipcMain.on('logMessage', this.messageHandler);
    }

    messageHandler(data) {
        messages.push({
            time: moment().format('LLL'),
            type: data.type,
            message: data.message
        })
    }

    getAllMessages() {
        return JSON.stringify(
            messages
        )
    }
}
