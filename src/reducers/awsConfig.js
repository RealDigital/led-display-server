const awsConfig_initial = {
    regionName: 'us-west-2',
    bucket: 'zeppoz-rdscreen'
}

const awsConfig = (state=awsConfig_initial, action) => {
    switch(action.type) {
        default:
            return state;
    }
}

export default awsConfig;
