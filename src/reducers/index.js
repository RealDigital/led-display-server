import { combineReducers } from 'redux';
import awsConfig from './awsConfig';
import content from './content';
import ledControl from './ledControl';
import displayConfiguration from './displayConfiguration';

const rootReducer = () => combineReducers({
    awsConfig,
    content,
    ledControl,
    displayConfiguration
});

export default rootReducer;
