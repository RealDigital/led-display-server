const displayConfiguration_initial = {
    width: 1024,
    height: 576,
    testMode: 'off',
    monocolorTest: {
        background: {
            hex: "#000",
            rgb: {
                r: 0,
                g: 0,
                b: 0
            }
        },
        contrast: 1.0,
        brightness: 1.0
    }
}

const displayConfiguration = (state=displayConfiguration_initial, action) => {
    switch(action.type) {
        default:
            return state;
    }
}

export default displayConfiguration;
