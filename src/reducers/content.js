import moment from 'moment-timezone';
import { ContentActions } from '../actions/content';

// Advertisement Content
const content_initial = {
    adContents: [],
    // Time configuration on display
    showTime: true,
    timeFormat: 'h:mm:ss A, MMMM DD, YYYY',
    showTimeFormatHelper: false,
    timeFormatHelperAnchor: undefined,
    timezone: moment.tz.guess(),
    timeTextBaseline: 'bottom',
    timeTextAlign: 'right',
    timeTextStrokeWidth: 3,
    timeTextStrokeColor: '#000',
    timeTextFillColor: '#fff',
    timeTextFontFamily: '',
    timeTextFontSize: 40,
    timeTextFontWeight: 400
}

const content = (state=content_initial, action) => {
    switch(action.type) {
        case ContentActions.LOAD_CONTENTS:
            return Object.assign({}, state, action.content);
        default:
            return state;
    }
}

export default content;
