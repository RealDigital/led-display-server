const ledControl_initial = {
    mode: 'timeBased',
    updateInterval: 60,
    constant: {
        brightness: 1.0,
        contrast: 1.0
    },
    timeBased: [{
        time: "06:00:00",
        brightness: 0.6,
        contrast: 1.0
    },{
        time: "08:00:00",
        brightness: 1.0,
        contrast: 0.7
    },{
        time: "16:00:00",
        brightness: 1.0,
        contrast: 0.7
    },{
        time: "17:30:00",
        brightness: 0.6,
        contrast: 1.0
    }],
    sensorBased: [
    ]
}

const valid_modes = ['none', 'constant', 'timeBased', 'sensorBased'];

const ledControl = (state=ledControl_initial, action) => {
    switch (action.type) {
        default:
            return state;
    }
}

export default ledControl;
