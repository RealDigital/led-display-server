export const ContentActions = {
    LOAD_CONTENTS: 'LOAD_CONTENTS'
}

export function loadContent(content) {
    return {
        type: ContentActions.LOAD_CONTENTS,
        content: content
    }
}
