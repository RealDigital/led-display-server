const { ipcMain } = require('electron');
const moment = require('moment-timezone');

/**
 * Server logging service
 */
const messages = [];

function loggingInit() {
    ipcMain.on('logMessage', (event, data) => {
        messages.push({
            time: moment().format(),
            type: data.type,
            message: data.message
        })
    });
}

function loggingReadAll() {
    return JSON.stringify(
        messages
    )
}

module.exports = {
    loggingInit,
    loggingReadAll
};
