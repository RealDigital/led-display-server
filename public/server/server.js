const { loggingInit, loggingReadAll } = require('./logging');
var serverApp = require('express')();
var bodyParser = require('body-parser');
serverApp.use(bodyParser.urlencoded({extended: false}));
serverApp.use(bodyParser.json());
let server;

function initServer() {
    loggingInit();

    serverApp.get('/', function(req, res) {
        res.send('Welcome to Real Digital Led Display Server.');
    })

    serverApp.get('/log', function(req, res) {
        res.send(loggingReadAll());
    })

    serverApp.post('/configDisplay', function(req, res) {
        console.log(req.body);
    })

    server = serverApp.listen(9000, function () {
        var host = server.address().address;
        host = (host === '::' ? 'localhost' : host);
        var port = server.address().port;
      
        console.log('listening at http://%s:%s', host, port);
    });
}

module.exports = {
    initServer
}
