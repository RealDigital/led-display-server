'use strict';

const { app, BrowserWindow, shell, ipcMain } = require('electron');
const os = require('os');
const path = require('path');
const isDev = require('electron-is-dev');
const { initServer } = require('./server/server');

// Keep a global reference of the window object. If you do not, the window will
// be closed automatically when the Javascript object is garbage collected.
let mainWindow = null;

// Allow autoplay without user interaction
app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');

// Mainwindow Creation
function createWindow() {
    mainWindow = new BrowserWindow({
        fullscreen: true,
        titlebarAppearsTransparent: true
    })

    mainWindow.loadURL(
        isDev 
        ? 'http://localhost:3000' 
        : `file://${path.join(__dirname, '../build/index.html')}`
    );

    if(isDev) {
        const {
            default: installExtension,
            REACT_DEVELOPER_TOOLS
        } = require('electron-devtools-installer');

        installExtension(REACT_DEVELOPER_TOOLS)
        .then(name => {
            console.log(`Added Extension: ${name}`);
        })
        .catch(err => {
            console.log('An error occurred: ', err);
        });

        // Open the devtools
        mainWindow.webContents.openDevTools();
    }

    // For distribution testing, open dev tools as well
    // mainWindow.webContents.openDevTools();

    initServer();
    
    // Once the content is ready to show, display the window
    mainWindow.once('ready-to-show', () => {
        mainWindow.show();

        ipcMain.on('open-external-window', (event, arg) => {
            shell.openExternal(arg);
        });
    })

    // Emitted when the window is closed
    mainWindow.on('closed', () => {
        // Dereference the window object. Usually you would store windows in an 
        // array if your app supports multi-windows. This is the time when you
        // should delete the corresponding element.
        mainWindow = null;
    })
}

// This method will be called when Electron has finished initialization and is
// ready to create browser windows. Some APIs can only be used after this event
// occurs.
app.on('ready', () => {
    createWindow();
})

app.on('window-all-closed', () => {
    // On macOS, it is common for applications and their menu bar to stay
    // active until the user quits explicitly with Cmd+Q.
    if (process.platform !== 'darwin') {
        app.quit();
    }
})

app.on('activate', () => {
    // On macOS, it is common to re-create a window in the app when the dock
    // icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
    }
})

// Check for memory usage every 30 minutes. If the app uses more than 70% of
// the system memory, relaunch.
// function getMemoryConsumption() {
//     var metrics = app.getAppMetrics();
//     var totalMemory = 0;
//     for(var i = 0; i < metrics.length; i++) {
//         let memory = metrics[i].memory;
//         totalMemory += memory.privateBytes;
//         console.log(memory);
//     }
//     console.log('App Mem: ' + totalMemory.toString());
//     console.log('Process Mem: ');
//     console.log(process.getProcessMemoryInfo());
//     console.log(process.getSystemMemoryInfo());
//     return totalMemory;
// }

// const osFreeMem = os.freemem();
// console.log('OS Free Mem: ' + osFreeMem.toString());
// const totalAllocableMem = os.freemem() + getMemoryConsumption();
// console.log('Allocable Mem: ' + osFreeMem.toString());

const minFreeMemory = Math.min(128000, 0.1*process.getSystemMemoryInfo().total);
console.log('Minimum free memory: ' + minFreeMemory.toString());

setInterval(() => {
    // console.log('Memory is ' + totalMemory.toString());
    // let totalMemory = getMemoryConsumption();
    let memory = process.getSystemMemoryInfo();

    // if(totalMemory > 0.7 * totalAllocableMem) {
    if(memory.free < minFreeMemory) {
        // console.log('Relaunch.');
        app.relaunch({ args: process.argv.slice(1).concat(['--relaunch']) })
        app.exit(0)
    }
//}, 1000);
}, 1800000);
